#!/bin/bash
node /usr/share/vfms/src/vfms-init.js
rm /usr/share/vfms/data/* 2>/dev/null
./user-tests.sh
./group-tests.sh
./acl-files-tests.sh
./read-file-tests.sh
./write-file-tests.sh
./limits-tests.sh
./length-tests.sh
node /usr/share/vfms/src/vfms-init.js
echo ""
