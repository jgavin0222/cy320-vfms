#!/bin/bash
rm /tmp/.vfms 2>/dev/null
vfms logout 2>/dev/null
vfms useradd grouptest password 2>/dev/null
vfms useradd grouptest2 password 2>/dev/null
vfms login grouptest password 2>/dev/null
create_test=$(vfms creategroup testgroup)
if [ -z "$create_test" ]; then
        echo -n "."
else
        echo -n "x"
fi
add_to_group_test=$(vfms addtogroup grouptest2 testgroup)
if [ -z "$add_to_group_test" ]; then
        echo -n "."
else
        echo -n "x"
fi
remove_from_group_test=$(vfms removefromgroup grouptest2 testgroup)
if [ -z "$remove_from_group_test" ]; then
        echo -n "."
else
        echo -n "x"
fi
delete_test=$(vfms deletegroup testgroup)
if [ -z "$delete_test"  ]; then
        echo -n "."
else
        echo -n "x"
fi
