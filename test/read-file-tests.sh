#!/bin/bash
rm /tmp/.vfms
vfms logout 2>/dev/null
vfms useradd read-tests pass
vfms useradd read-tests-2 pass
vfms login read-tests pass
vfms createfile file-to-read data
read_basic_test=$(vfms readfile file-to-read)
if [[ "$read_basic_test" == "data" ]];then
	echo -n "."
else
	echo -n "x"
fi
vfms login read-tests-2 pass 2>/dev/null
read_second_user_before_grant_test=$(vfms readfile file-to-read)
if [[ "$read_second_user_before_grant_test" == "You cannot read this file!" ]]; then
        echo -n "."
else
        echo -n "x"
fi
vfms login read-tests pass 2>/dev/null
vfms grantread read-tests-2 file-to-read 2>/dev/null
vfms login read-tests-2 pass 2>/dev/null
read_second_user_test=$(vfms readfile file-to-read)
if [[ "$read_second_user_test" == "data" ]];then
        echo -n "."
else
        echo -n "x"
fi
vfms login read-tests pass 2>/dev/null
vfms revokeread read-tests-2 file-to-read 2>/dev/null
vfms login read-tests-2 pass
read_second_user_after_revoke_test=$(vfms readfile file-to-read)
if [[ "$read_second_user_after_revoke_test" == "You cannot read this file!" ]]; then
	echo -n "."
else
	echo -n "x"
fi
vfms login read-tests pass 2>/dev/null
vfms creategroup read-file-tests-group 2>/dev/null
vfms addtogroup read-tests-2 read-file-tests-group 2>/dev/null
vfms grantgroupread read-file-tests-group file-to-read 2>/dev/null
vfms login read-tests-2 pass 2>/dev/null
read_with_group_test=$(vfms readfile file-to-read)
if [[ "$read_with_group_test" == "data" ]];then
	echo -n "."
else
	echo -n "x"
fi
vfms login read-tests pass 2>/dev/null
vfms revokegroupread read-file-tests-group file-to-read 2>/dev/null
vfms login read-tests-2 pass 2>/dev/null
read_with_group_after_revoked_test=$(vfms readfile file-to-read)
if [[ "$read_second_user_after_revoke_test" == "You cannot read this file!" ]]; then
        echo -n "."
else
        echo -n "x"
fi
vfms login read-tests pass 2>/dev/null
vfms deletefile file-to-read 2>/dev/null
