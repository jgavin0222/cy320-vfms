#!/bin/bash
vfms logout 2>/dev/null
vfms useradd aclfilestest pass 2>/dev/null
vfms login aclfilestest pass 2>/dev/null
vfms createfile test-acl data 2>/dev/null
acl_test=$(vfms viewacl test-acl)
if [[ "$acl_test" == "aclfilestest owns this file" ]]; then
	echo -n "."
else
	echo -n "x"
fi
view_files_test=$(vfms viewfiles)
if [[ "$view_files_test" == "test-acl" ]]; then
	echo -n "."
else
	echo -n "x"
fi
vfms deletefile test-acl 2>/dev/null
