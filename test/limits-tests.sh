#!/bin/bash
vfms logout 2>/dev/null
rm -f ../data/*
node ../src/vfms-init.js
vfms useradd test test
vfms login test test
vfms createfile file1 data
vfms createfile file2 data
vfms createfile file3 data
vfms createfile file4 data
vfms createfile file5 data
vfms createfile file6 data
vfms createfile file7 data
vfms createfile file8 data
vfms createfile file9 data
vfms createfile file10 data
limits_test=$(vfms createfile file11 data)
if [[ "$limits_test" == "VFMS is currently controlling the maximum number of files (10)" ]]; then
 echo -n "."
else
 echo -n "x"
fi
