#!/bin/bash
rm /tmp/.vfms 2>/dev/null
vfms logout 2>/dev/null
create_test=$(vfms useradd test password)
if [ -z "$create_test" ]; then
	echo -n "."
else
	echo -n "x"
fi
login_test=$(vfms login test password)
if [ -z "$login_test" ]; then
        echo -n "."
else
        echo -n "x"
fi
logout_test=$(vfms logout)
if [ -z "$logout_test" ]; then
        echo -n "."
else
        echo -n "x"
fi
delete_test=$(vfms userdel test password)
if [[ "$delete_test" == "Removing user test" ]]; then
        echo -n "."
else
        echo -n "x"
fi
