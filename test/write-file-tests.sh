#!/bin/bash
rm /tmp/.vfms 2>/dev/null
vfms logout 2>/dev/null
vfms useradd write-tests pass
vfms useradd write-tests-2 pass
vfms login write-tests pass
vfms createfile file-to-write data
write_basic_test=$(vfms writefile file-to-write data)
if [ -z "$write_basic_test" ];then
	echo -n "."
else
	echo -n "x"
fi
vfms login write-tests-2 pass 2>/dev/null
write_second_user_before_grant_test=$(vfms writefile file-to-write data2)
if [[ "$write_second_user_before_grant_test" == "You cannot write this file!" ]]; then
        echo -n "."
else
        echo -n "x"
fi
vfms login write-tests pass 2>/dev/null
vfms grantwrite write-tests-2 file-to-write 2>/dev/null
vfms login write-tests-2 pass 2>/dev/null
write_second_user_test=$(vfms writefile file-to-write data2)
if [ -z "$write_second_user_test" ];then
        echo -n "."
else
        echo -n "x"
fi
vfms login write-tests pass 2>/dev/null
vfms revokewrite write-tests-2 file-to-write 2>/dev/null
vfms login write-tests-2 pass
write_second_user_after_revoke_test=$(vfms writefile file-to-write data2)
if [[ "$write_second_user_after_revoke_test" == "You cannot write this file!" ]]; then
	echo -n "."
else
	echo -n "x"
fi
vfms login write-tests pass 2>/dev/null
vfms creategroup write-file-tests-group 2>/dev/null
vfms addtogroup write-tests-2 write-file-tests-group 2>/dev/null
vfms grantgroupwrite write-file-tests-group file-to-write 2>/dev/null
vfms login write-tests-2 pass 2>/dev/null
write_with_group_test=$(vfms writefile file-to-write data3)
if [ -z "$write_with_group_test" ];then
	echo -n "."
else
	echo -n "x"
	echo "$write_with_group_test"
fi
vfms login write-tests pass 2>/dev/null
vfms revokegroupread write-file-tests-group file-to-write 2>/dev/null
vfms login write-tests-2 pass 2>/dev/null
write_with_group_after_revoked_test=$(vfms writefile file-to-write data4)
if [[ "$write_second_user_after_revoke_test" == "You cannot write this file!" ]]; then
        echo -n "."
else
        echo -n "x"
fi
vfms login write-tests pass 2>/dev/null
vfms deletefile file-to-write 2>/dev/null
