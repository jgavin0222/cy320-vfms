const express = require('express');
const bodyParser = require('body-parser')
const vfmsUser = require('/usr/share/vfms/src/vfms-user')
const vfmsCore = require('/usr/share/vfms/src/vfms-core')
const app = express();
const port = 3000;

app.use(bodyParser.json())
app.get('/',(req,res)=>{
	var data = {
		"status":"success",
		"name":"VFMS by Fighting Chicken"
	}
	res.status(200).send(data);
});

app.post('/auth',(req,res)=>{
	var { username, password } = req.body;
	if(username && password){
		var token = vfmsUser.login(username,password,true);
		var base64Token = new Buffer.from(token).toString('base64');
		res.status(200).send({"status":"success","data":base64Token});
	}else{
		res.status(400).send({"status":"error","error":"missing username or password"});
	}
});

app.post('/user/add',(req,res)=>{
	var { username, password } = req.body;
	if(username && password){
		var result = vfmsUser.create(username,password,true);
		if(result){
			res.status(200).send({"status":"success"});
		}else{
			res.status(400).send({"status":"error","error":"user already exists"})
		}
	}else{
		res.stats(400).send({"status":"error","error":"missing username or password"});
	}
});

app.post('/user/del',(req,res)=>{
	var { username, password } = req.body;
	if(username && password){
		var result = vfmsUser.remove(username,password,true);
		if(result){
			res.status(200).send({"status":"success"});
		}else{
			res.status(400).send({"status":"error","error":"could not find user with given credentials"});
		}
	}else{
		res.status(400).send({"status":"error","error":"missing username or password"});
	}
});

app.post('/file',(req,res)=>{
	var { authorization } = req.headers;
	var { location, content } = req.body;
	var jsonAuth = new Buffer.from(authorization,'base64').toString('ascii');
	if(vfmsUser.validate(jsonAuth)){
		if(location && content){
			var create = vfmsCore.createFile(location, content, true);
			if(create){
				res.status(200).send({"status":"success"});
			}else{
				res.status(400).send({"status":"error","error":"file already exists, use writefile"});
			}
		}else{
			res.status(400).send({"status":"error","error":"missing location or content"});
		}
	}else{
		res.status(401).send({"status":"error"});
	}
});

console.log("Server is listening on port "+port);
app.listen(port);
