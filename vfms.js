const fs = require('fs');
//const vfmsUser = require('/usr/share/vfms/src/vfms-user');
//const vfmsCrypto = require('/usr/share/vfms/src/vfms-crypto');
//const vfmsCore = require('/usr/share/vfms/src/vfms-core');

const acl = '/usr/share/vfms/config/acl.config'
const auth = '/usr/share/vfms/config/auth.config'

const vfmsUser = require('./src/vfms-user.js');
const vfmsCrypto = require('./src/vfms-crypto.js');
const vfmsCore = require('./src/vfms-core.js');

var dacl = fs.readFileSync(acl, { encoding:'utf8', flag:'r' });
var ddata = fs.readFileSync(auth, { encoding:'utf8', flag:'r' });

var jsonDacl = JSON.parse(dacl);
var jsonDdata = JSON.parse(ddata);

help = () => {
	console.log("login <username> <password>")
	console.log("logout")
	console.log("useradd <username> <password>")
	console.log("userdel <username> <password")
	console.log("createfile <filename> <content>")
	console.log("deletefile <filename>")
	console.log("readfile <filename>")
	console.log("writefile <filename> <content>")
	console.log("grantread <username> <filename>")
	console.log("grantwrite <username> <filename>")
	console.log("revokeread <username> <filename>")
	console.log("revokewrite <username> <filename>")
	console.log("grantgroupread <groupname> <filename>")
	console.log("grantgroupwrite <groupname> <filename>")
	console.log("revokegroupread <groupname> <filename>")
	console.log("revokegroupwrite <groupname> <filename>")
	console.log("creategroup <groupname>")
	console.log("deletegroup <groupname>")
	console.log("addtogroup <username> <group>")
	console.log("removefromgroup <username> <group>")
	console.log("viewacl <filename>")
	console.log("viewfiles")
}

var myArgs = process.argv.slice(2);
if(myArgs[0] == 'login'){
	if(myArgs.length != 3){console.log("login <username> <password>");}else{vfmsUser.login(myArgs[1],myArgs[2])}
}else if(myArgs[0] == 'logout'){
	vfmsUser.logout()
}else if(myArgs[0] == 'useradd'){
	if(myArgs.length != 3){console.log("useradd <username> <password>");}else{vfmsUser.create(myArgs[1],myArgs[2]);}
}else if(myArgs[0] == 'userdel'){
	if(myArgs.length != 3){console.log("userdel <username> <password>");}else{vfmsUser.remove(myArgs[1],myArgs[2]);}
}else if(myArgs[0] == 'createfile'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("createfile <filename> <content>")}else{
			var name = myArgs[1];
			myArgs.shift();
			myArgs.shift();
			var allArgs = myArgs.join(" ");
			vfmsCore.createFile(name,allArgs);
		}
	}
}else if(myArgs[0] == 'deletefile'){
	if(vfmsUser.validate()){
		if(myArgs.length < 2){console.log("deletefile <filename>")}else{
			var name = myArgs[1];
			vfmsCore.deleteFile(name);
		}
	}
}else if(myArgs[0] == 'writefile'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("writefile <filename> <content>")}else{
			var name = myArgs[1];
			myArgs.shift();
			myArgs.shift();
			var allArgs = myArgs.join(" ");
			vfmsCore.writeFile(name,allArgs);
		}
	}
}else if(myArgs[0] == 'readfile'){
	if(vfmsUser.validate()){
		if(myArgs.length < 2){console.log("readfile <filename>")}else{
			vfmsCore.readFile(myArgs[1]);
		}
	}
}else if(myArgs[0] == 'grantread'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("grantread <username> <filename>")}else{
			vfmsCore.grantRead(myArgs[2],myArgs[1]);
		}
	}
}else if(myArgs[0] == 'grantwrite'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("grantwrite <username> <filename>")}else{
			vfmsCore.grantWrite(myArgs[2],myArgs[1]);
		}
	}
}else if(myArgs[0] == 'revokeread'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("revokeread <username> <filename>")}else{
			vfmsCore.revokeRead(myArgs[2],myArgs[1]);
		}
	}
}else if(myArgs[0] == 'revokewrite'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("revokewrite <username> <filename>")}else{
			vfmsCore.revokeWrite(myArgs[2],myArgs[1]);
		}
	}
}else if(myArgs[0] == 'grantgroupread'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("grantgroupread <groupname> <filename>")}else{
			vfmsCore.grantGroupRead(myArgs[1],myArgs[2]);
		}
	}
}else if(myArgs[0] == 'grantgroupwrite'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("grantgroupwrite <groupname> <filename>")}else{
			vfmsCore.grantGroupWrite(myArgs[1],myArgs[2]);
		}
	}
}else if(myArgs[0] == 'revokegroupread'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("revokegroupread <groupname> <filename>")}else{
                        vfmsCore.revokeGroupRead(myArgs[2],myArgs[1]);
                }
	}
}else if(myArgs[0] == 'revokegroupwrite'){
	if(vfmsUser.validate()){
                if(myArgs.length < 3){console.log("revokegroupwrite <groupname> <filename>")}else{
                        vfmsCore.revokeGroupWrite(myArgs[2],myArgs[1]);
                }
        }
}else if(myArgs[0] == 'creategroup'){
	if(vfmsUser.validate()){
		if(myArgs.length < 2){console.log("creategroup <groupname>")}else{
			vfmsUser.createGroup(myArgs[1])
		}
	}
}else if(myArgs[0] == 'deletegroup'){
	if(vfmsUser.validate()){
                if(myArgs.length < 2){console.log("deletegroup <groupname>")}else{
                        vfmsUser.deleteGroup(myArgs[1])
                }
        }
}else if(myArgs[0] == 'addtogroup'){
	if(vfmsUser.validate()){
		if(myArgs.length < 3){console.log("addtogroup <username> <group>")}else{
			vfmsUser.addToGroup(myArgs[1],myArgs[2])
		}
	}
}else if(myArgs[0] == 'removefromgroup'){
	if(vfmsUser.validate()){
                if(myArgs.length < 3){console.log("removefromgroup <username> <group>")}else{
                        vfmsUser.removeFromGroup(myArgs[1],myArgs[2])
                }
        }

}else if(myArgs[0] == 'viewacl'){
	if(myArgs.length < 2){console.log("viewacl <filename>")}else{
		vfmsCore.viewAcl(myArgs[1]);
	}
}else if(myArgs[0] == 'viewfiles'){
	var files = fs.readdirSync('/usr/share/vfms/data/')
	files.map(data=>console.log(data))
}else{
	help();
}
