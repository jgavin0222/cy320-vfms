const fs = require('fs');
const vfmsCrypto = require('/usr/share/vfms/src/vfms-crypto');

const acl = '/usr/share/vfms/config/acl.config'
const auth = '/usr/share/vfms/config/auth.config'

var dacl = fs.readFileSync(acl, { encoding:'utf8', flag:'r' });
var ddata = fs.readFileSync(auth, { encoding:'utf8', flag:'r' });

console.log('acl', dacl);
console.log('auth', ddata);
