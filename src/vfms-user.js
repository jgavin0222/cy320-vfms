const fs = require('fs');
const vfmsCrypto = require('./vfms-crypto');

const auth = '/usr/share/vfms/config/auth.config'

//fs.writeFileSync(auth,JSON.stringify({}))

create = (username, password, web) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth);
	var jsonCurrentAuth = JSON.parse(currentAuth);
	var found = false;
	jsonCurrentAuth.users.map(user=>{
		if(user.username == username){
			found=true;
		}
	});
	if(!found){
		var passwordHash = vfmsCrypto.hash(password);
		var user = { username, passwordHash };
		jsonCurrentAuth.users.push(user);
		var currentAuthJson = JSON.stringify(jsonCurrentAuth);
		fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson));
		if(web){ return true; }
	}else{
		if(web){
			return false;
		}else{
			console.log("User already exists")
		}
	}
}

remove = (username, password, web) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth);
	var jsonCurrentAuth = JSON.parse(currentAuth);
	var keepUsers = [];
	var found = false;
	jsonCurrentAuth.users.map(user=>{
		if(user.username == username){
			if(user.passwordHash == vfmsCrypto.hash(password)){
				found = true
				if(!web){
					console.log("Removing user "+username)
				}
			}else{
				console.log("Incorrect password")
				keepUsers.push(user)
			}
		}else{
			keepUsers.push(user)
		}
	});
	jsonCurrentAuth.users = keepUsers
	var currentAuthJson = JSON.stringify(jsonCurrentAuth);
	fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson))
	if(web){
		return found;
	}
}

createGroup = (groupname) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth);
        var jsonCurrentAuth = JSON.parse(currentAuth);
	jsonCurrentAuth.groups.push({"owner":username(),"name":groupname,users:[]});
	var currentAuthJson = JSON.stringify(jsonCurrentAuth);
        fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson))
}

deleteGroup = (groupname) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
        currentAuth = vfmsCrypto.decrypt(currentAuth);
	var jsonCurrentAuth = JSON.parse(currentAuth);
	var keepGroups = []
	jsonCurrentAuth.groups.map(group => {
		if(group.name == groupname){
			if(group.owner !== username()){
				keepGroups.push(group);
				console.log("You do not own this group");
			}
		}else{
			keepGroups.push(group);
		}
	});
	jsonCurrentAuth.groups = keepGroups;
	var currentAuthJson = JSON.stringify(jsonCurrentAuth);
        fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson))
}

addToGroup = (user,groupname) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth);
        var jsonCurrentAuth = JSON.parse(currentAuth)
	jsonCurrentAuth.groups.map(group=>{
		if(group.name == groupname){
			if(group.owner == username()){
				group.users.push(user);
			}else{
				console.log("You do not own this group");
			}
		}
	});
	var currentAuthJson = JSON.stringify(jsonCurrentAuth);
        fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson))
}

removeFromGroup = (user,groupname) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth)
        var jsonCurrentAuth = JSON.parse(currentAuth)
        jsonCurrentAuth.groups.map(group=>{
                if(group.name == groupname){
                        if(group.owner == username()){
				var keepUsers = []
				group.users.map(groupUser => {
					if(groupUser !== user){
						keepUsers.push(groupUser);
					}
				});
				group.users = keepUsers;
			}
		}
	});
	var currentAuthJson = JSON.stringify(jsonCurrentAuth);
        fs.writeFileSync(auth, vfmsCrypto.encrypt(currentAuthJson))
}

login = (username, password, webReturn) => {
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth)
	var jsonCurrentAuth = JSON.parse(currentAuth)
	var found = false
	var token = {};
	jsonCurrentAuth.users.map(user=>{
		if(user.username == username && user.passwordHash == vfmsCrypto.hash(password)){
			var hash = vfmsCrypto.hash(password)
			var obj = { username, hash:vfmsCrypto.hash(username+':'+hash) }
			var jsonUser = JSON.stringify(obj)
			found = true
			if(!webReturn){
				fs.writeFileSync('/tmp/.vfms',jsonUser)
				fs.chmodSync('/tmp/.vfms', 0o600)
			}else{
				token = jsonUser;
			}
		}
	});
	if(!found){
		if(!webReturn){
			console.log("Invalid Username or Password!")
		}else{
			return false;
		}
	}else{
		if(webReturn){
			return token;
		}
	}
}

validate = (token) => {
	var user = {};
	if(!token){
		try{
			user = fs.readFileSync('/tmp/.vfms', { encoding:'utf8', flag:'r' })
		}catch(err){
			console.log("Error reading file")
			return false;
		}
	}else{
		user=token;
	}
	var jsonUser = JSON.parse(user)
	var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
	currentAuth = vfmsCrypto.decrypt(currentAuth)
	var jsonCurrentAuth = JSON.parse(currentAuth);
	var valid = false;
	jsonCurrentAuth.users.map(user=>{
		if(user.username == jsonUser.username){
			if(vfmsCrypto.hash(user.username+':'+user.passwordHash) == jsonUser.hash){
				valid = true;
			}
		}
	});
	if(!valid){
		console.log("Please log in")
	}
	return valid;
}

logout = () => {
	fs.unlinkSync('/tmp/.vfms')
}

username = () => {
	var authData = fs.readFileSync('/tmp/.vfms',{ encoding:'utf8', flag:'r' });
	var authDataJson = JSON.parse(authData);
	return authDataJson.username
}

module.exports = {
	create,
	remove,
	login,
	logout,
	validate,
	username,
	createGroup,
	deleteGroup,
	addToGroup,
	removeFromGroup
}
