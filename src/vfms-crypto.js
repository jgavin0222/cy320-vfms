const fs = require('fs');
const crypto = require('crypto');

const algorithm = 'aes-256-ctr';
let key = 'georgeTeachesCY320';
key = crypto.createHash('sha256').update(key).digest('base64').substr(0,32);
const hmac = crypto.createHmac("sha256",key)
const encrypt = (buffer) => {
	const iv = crypto.randomBytes(16);
	const cipher = crypto.createCipheriv(algorithm, key, iv);
	const result = Buffer.concat([cipher.update(buffer.toString('base64')), cipher.final()]);
	return JSON.stringify({ iv: iv.toString('hex'), content: result.toString('hex') });
}

const decrypt = (encrypted) => {
	encrypted = JSON.parse(encrypted);
	const iv = Buffer.from(encrypted.iv,'hex');
	const decipher = crypto.createDecipheriv(algorithm, key, iv);
	const result = Buffer.concat([decipher.update(Buffer.from(encrypted.content,'hex')), decipher.final()]);
	return result.toString('ascii');
}

const hash = (data) => {
	const hmac = crypto.createHmac("sha256",key)
	var final = hmac.update(data).digest('hex')
	return final
}

module.exports = { encrypt, decrypt, hash };
