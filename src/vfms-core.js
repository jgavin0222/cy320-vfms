const fs = require('fs');
const vfmsUser = require('./vfms-user')
const vfmsCrypto = require('./vfms-crypto')

const acl = '/usr/share/vfms/config/acl.config';
const auth = '/usr/share/vfms/config/auth.config';

createFile = (location, content, web) => {
	var truepath = '/usr/share/vfms/data/'+location
	if(!fs.existsSync(truepath)){
		//fs.writeFileSync(truepath,vfmsCrypto.encrypt(content));
		var contentValue = vfmsCrypto.encrypt(content);
		if(Buffer.byteLength(contentValue) > 1024){
			console.log("Content exceeds 1024 bytes")
			return false;
		}
		if(fs.readdirSync('/usr/share/vfms/data/').length >= 10){
			console.log("VFMS is currently controlling the maximum number of files (10)")
			return false;
		}
		var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl)
		var aclJson = JSON.parse(currentAcl);
		aclJson.files.push({"owner":vfmsUser.username(),"location":location,read:[],write:[],groupread:[],groupwrite:[]});
		fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
		fs.writeFileSync(truepath,contentValue);
		fs.chmodSync(truepath, 0o600)
		if(web){ return true; }
	}else{
		if(web){
			return false;
		}else{
			console.log("File exists, use writefile")
		}
	}
}

deleteFile = (location) => {
	var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
	currentAcl = vfmsCrypto.decrypt(currentAcl)
        var aclJson = JSON.parse(currentAcl);
	var keepAcl = [];
	aclJson.files.map(file => {
		if(file.location == location){
			if(file.owner == vfmsUser.username()){
				fs.unlinkSync('/usr/share/vfms/data/'+location);
			}else{
				keepAcl.push(file);
				console.log("You do not own this file");
			}
		}else{
			keepAcl.push(file);
		}
	});
	aclJson.files = keepAcl;
	fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
}

writeFile = (location, content) => {
        var truepath = '/usr/share/vfms/data/'+location
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
        	currentAcl = vfmsCrypto.decrypt(currentAcl)
		var aclJson = JSON.parse(currentAcl);
        	aclJson.files.map(file => {
                	if(file.location == location){
				var write=false;
                        	if(file.owner == vfmsUser.username()){
                                	write=true;
                        	}
				file.write.map(user=>{
					if(user == vfmsUser.username()){
						write=true;
					}
				});
				var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
        			currentAuth = vfmsCrypto.decrypt(currentAuth);
				var authJson = JSON.parse(currentAuth);
				file.groupwrite.map(group => {
                                        authJson.groups.map(groupAuth => {
                                                if(groupAuth.name == group){
                                                        if(groupAuth.owner == vfmsUser.username()){
                                                                write=true;
                                                        }
                                                        groupAuth.users.map(groupAuthUser => {
                                                                if(groupAuthUser == vfmsUser.username()){
                                                                        write=true;
                                                                }
                                                        })
                                                }
                                        });
                                });
				if(write){
					var contentValue = vfmsCrypto.encrypt(content);
					if(Buffer.byteLength(contentValue) > 1024){
						console.log("File would exceed 1024 bytes, not writing");
					}else{
						fs.writeFileSync('/usr/share/vfms/data/'+location,contentValue);
						fs.chmodSync('/usr/share/vfms/data/'+location, 0o600);
					}
				}else{
					console.log("You cannot write this file!");
				}
               		}
        	});
        }else{
                console.log("File does not exist, use createfile")
        }
}

readFile = (location) => {
        var truepath = '/usr/share/vfms/data/'+location
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl)
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                var read=false;
                                if(file.owner == vfmsUser.username()){
                                        read=true;
                                }
                                file.read.map(user=>{
                                        if(user == vfmsUser.username()){
                                                read=true;
                                        }
                                });
				var currentAuth = fs.readFileSync(auth, { encoding:'utf8', flag:'r' })
				currentAuth = vfmsCrypto.decrypt(currentAuth);
                                var authJson = JSON.parse(currentAuth);
                                file.groupread.map(group => {
					authJson.groups.map(groupAuth => {
						if(groupAuth.name == group){
                                        		if(groupAuth.owner == vfmsUser.username()){
                                                		read=true;
                                        		}
                                        		groupAuth.users.map(groupAuthUser => {
                                                		if(groupAuthUser == vfmsUser.username()){
                                                        		read=true;
                                                		}
                                        		})
						}
					});
                                });
                                if(read){
                                        console.log(vfmsCrypto.decrypt(fs.readFileSync('/usr/share/vfms/data/'+location,{ encoding:'utf8', flag:'r' })));
                                }else{
                                        console.log("You cannot read this file!");
                                }
                        }
                });
        }else{
                console.log("File does not exist, use createfile")
        }
}

grantRead = (location, user) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl)
                var aclJson = JSON.parse(currentAcl);
		aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        file.read.push(user);
                                }else{
					console.log("You do not own this file")
				}
			}
		});
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

grantWrite = (location, user) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        file.write.push(user);
                                }else{
					console.log("You do not own this file")
				}
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

revokeRead = (location, user) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
		aclJson.files.map(file => {
	                if(file.location == location){
        	                if(file.owner == vfmsUser.username()){
					var validReadUsers = [];
					file.read.map(readUser => {
						if(readUser !== user){
							validReadUsers.push(readUser);
						}
					});
					file.read = validReadUsers;
				}else{
					console.log("You do not own this file")
				}
			}
		});
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

revokeWrite = (location, user) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        var validWriteUsers = [];
                                        file.write.map(writeUser => {
                                                if(writeUser !== user){
                                                        validWriteUsers.push(writeUser);
                                                }
                                        });
                                        file.write = validWriteUsers;
                                }else{
                                        console.log("You do not own this file")
                                }
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

grantGroupRead = (groupname,location) => {
	var truepath = '/usr/share/vfms/data/'+location;
	if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        file.groupread.push(groupname);
                                }else{
                                        console.log("You do not own this file")
                                }
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

grantGroupWrite = (groupname,location) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        file.groupwrite.push(groupname);
                                }else{
                                        console.log("You do not own this file")
                                }
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

revokeGroupRead = (location, group) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        var validGroupReadUsers = [];
                                        file.groupread.map(readGroup => {
                                                if(readGroup !== group){
                                                        validReadUsers.push(readGroup);
                                                }
                                        });
                                        file.groupread = validGroupReadUsers;
                                }else{
                                        console.log("You do not own this file")
                                }
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}

revokeGroupWrite = (location, group) => {
        var truepath = '/usr/share/vfms/data/'+location;
        if(fs.existsSync(truepath)){
                var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
		currentAcl = vfmsCrypto.decrypt(currentAcl);
                var aclJson = JSON.parse(currentAcl);
                aclJson.files.map(file => {
                        if(file.location == location){
                                if(file.owner == vfmsUser.username()){
                                        var validGroupWriteUsers = [];
                                        file.groupwrite.map(writeGroup => {
                                                if(writeGroup !== group){
                                                        validGroupWriteUsers.push(writeGroup);
                                                }
                                        });
                                        file.writegroup = validGroupWriteUsers;
                                }else{
                                        console.log("You do not own this file")
                                }
                        }
                });
                fs.writeFileSync(acl,vfmsCrypto.encrypt(JSON.stringify(aclJson)));
        }else{
                console.log("File does not exist, use createfile")
        }
}


viewAcl = (location) => {
	var currentAcl = fs.readFileSync(acl,{ encoding:'utf8', flag:'r' });
	currentAcl = vfmsCrypto.decrypt(currentAcl);
        var aclJson = JSON.parse(currentAcl);
        var keepAcl = [];
        aclJson.files.map(file => {
		if(file.location == location){
			console.log(file.owner+" owns this file");
			file.read.map(user=>{
				console.log(user+" may read this file");
			});
			file.write.map(user=>{
				console.log(user+" may write this file");
			});
			file.groupread.map(group=>{
				console.log("Members of the "+group+" group may read this file");
			});
			file.groupwrite.map(group=>{
				console.log("Members of the "+group+" group may write this file");
			});
		}
	});
}

module.exports = {
	createFile,
	deleteFile,
	writeFile,
	readFile,
	grantRead,
	grantWrite,
	revokeRead,
	revokeWrite,
	viewAcl,
	grantGroupRead,
	grantGroupWrite,
	revokeGroupRead,
	revokeGroupWrite
}
