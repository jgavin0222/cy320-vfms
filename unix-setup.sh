#!/bin/bash
apt-get update
apt-get install -y nodejs
echo 'vfms:x:320:320:vfms by the fighting chicken:/usr/share/vfms:/sbin/noglogin'>>/etc/passwd
echo 'vfms:x:320:vfms'>>/etc/group
chown vfms:vfms /usr/share/vfms
chmod 600 /usr/share/vfms
mkdir /usr/share/vfms/data
chmod 600 /usr/share/vfms/config
chmod 600 /usr/share/vfms/data
chown vfms:vfms /usr/share/vfms/config
chown vfms:vfms /usr/share/vfms/data
echo 'ALL     ALL = (vfms) /usr/share/vfms/vfms'>>/etc/sudoers
touch /usr/share/vfms/config/acl.config
chmod 600 /usr/share/vfms/config/acl.config
chown vfms:vfms /usr/share/vfms/config/acl.config
touch /usr/share/vfms/config/auth.config
chmod 600 /usr/share/vfms/config/auth.config
chown vfms:vfms /usr/share/vfms/config/auth.config
node /usr/share/vfms/src/vfms-init.js
cp /usr/share/vfms/config/utils/vfms /usr/bin/vfms
chmod +x /usr/bin/vfms
rm -rf /usr/share/vfms/src/
rm -rf /usr/share/vfms/test/
rm -f /usr/share/vfms/vfms.js
rm -f /usr/share/vfms/server.js
rm -f /usr/share/vfms/package.json
rm -f /usr/share/vfms/vfms.js
rm -f /usr/share/vfms/generate-bin.sh
chown vfms:vfms -R /usr/share/vfms/
chmod -R 700 /usr/share/vfms
rm -f /usr/share/vfms/unix-setup.sh
